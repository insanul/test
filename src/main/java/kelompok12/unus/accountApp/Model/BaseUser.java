package kelompok12.unus.accountApp.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "user")
public class BaseUser implements User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String userName;

    @Column(nullable = false)
    private String password;

    private boolean active;

    private String roles = "";

    public BaseUser(String userName, String password, String roles){
        this.userName = userName;
        this.password = password;
        this.active = true;
        this.roles = roles;
    }

    protected BaseUser(){
    }

    public long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public boolean isActive() {
        return active;
    }

    public String getRoles() {
        return roles;
    }

    public List<String> getRolesList(){
        if (roles.length() > 0) return Arrays.asList(roles.split(","));
        return new ArrayList<>();
    }
}
