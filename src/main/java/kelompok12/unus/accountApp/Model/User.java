package kelompok12.unus.accountApp.Model;

import java.util.List;

public interface User {
    public long getId();
    public String getUserName();
    public String getPassword();
    public boolean isActive();
    public String getRoles();
    public List<String> getRolesList();
}
