package kelompok12.unus.accountApp.Controller;

import kelompok12.unus.accountApp.Model.BaseUser;
import kelompok12.unus.accountApp.Model.User;
import kelompok12.unus.accountApp.Repository.UserRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/account")

public class AccountController {
    private UserRepository userRepository;

    public AccountController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
}
