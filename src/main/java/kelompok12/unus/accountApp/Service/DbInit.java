package kelompok12.unus.accountApp.Service;

import kelompok12.unus.accountApp.Model.BaseUser;
import kelompok12.unus.accountApp.Repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class DbInit implements CommandLineRunner {
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    public DbInit(UserRepository userRepository, PasswordEncoder passwordEncoder){
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) throws Exception {
        BaseUser admin = new BaseUser("admin", passwordEncoder.encode("admin123"), "ADMIN,USER");

        List<BaseUser> users = Arrays.asList(admin);

        this.userRepository.saveAll(users);
    }
}
