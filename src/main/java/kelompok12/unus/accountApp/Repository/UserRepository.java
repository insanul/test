package kelompok12.unus.accountApp.Repository;

import kelompok12.unus.accountApp.Model.BaseUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<BaseUser, Long> {
    BaseUser findByUserName(String userName);
}