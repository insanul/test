package kelompok12.unus.dompetApp.Core;

public class Dompet {
    private String name;
    private String desc;
    private int total;

    public Dompet(String nama, String desc){
        this.name = nama;
        this.desc = desc;
        total = 0;
    }

    public String getName(){
        return name;
    }

    
    public String getDescription(){
        return desc;
    }
}
