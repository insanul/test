package kelompok12.unus.dompetApp.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(path = "/dompet")
public class DompetController {
    @RequestMapping(method = RequestMethod.GET, value = "/")
    private String dompetMethod1() {
        return "home";
    }
    @RequestMapping(method = RequestMethod.GET, value = "/splitbill")
    private String dompetMethod2() {
        return "SplitBill";
    }
}
