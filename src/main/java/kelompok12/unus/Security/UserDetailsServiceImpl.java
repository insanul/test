package kelompok12.unus.Security;

import kelompok12.unus.accountApp.Model.BaseUser;
import kelompok12.unus.accountApp.Model.User;
import kelompok12.unus.accountApp.Repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        BaseUser user = this.userRepository.findByUserName(username);
        UserDetailsImpl userDetails = new UserDetailsImpl(user);
        return userDetails;
    }
}
