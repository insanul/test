package kelompok12.unus.notifierSbApp.Repository;

import kelompok12.unus.notifierSbApp.Core.NotifModel;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class NotifRepo {
    private Map<String, NotifModel> notifList = new HashMap<>();

    public Map<String, NotifModel> getNotifications() {
        return notifList;
    }

    public NotifModel save(NotifModel savedNotif) {
        NotifModel notifExisted = notifList.get(savedNotif.getTitle());
        if (notifExisted == null) {
            notifList.put(savedNotif.getTitle(), savedNotif);
            return savedNotif;
        } else {
            return null;
        }
    }
}
