package kelompok12.unus.notifierSbApp.Controller;

import kelompok12.unus.notifierSbApp.Core.NotifModel;
import kelompok12.unus.notifierSbApp.Core.Person;
import kelompok12.unus.notifierSbApp.Service.DBAccNotifService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class NsbController {
    Person orang = new Person();

    @RequestMapping(method = RequestMethod.GET, value = "/split-bill")
    private String splitBill() {
        return "SplitBill";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/notif")
    private String notif(Model model) {
        model.addAttribute("notifList", orang.getNotifs());
        return "Notification";
    }

    @RequestMapping(value = "/notif/create-notif", method = RequestMethod.GET)
    public String createNotif(Model model){
        model.addAttribute("newNotif", new NotifModel());
        return "notifForm";
    }

    @RequestMapping(value = "/notif/add-notif", method = RequestMethod.POST)
    public String addNotif(@ModelAttribute("notificat") NotifModel notifica) {
        orang.addNotif(notifica);
        return "redirect:/notif";
    }

    @Autowired
    private DBAccNotifService accNotifService;

    @RequestMapping(value = "/notif-dev", method = RequestMethod.GET)
    public String getAccounts(Model model){
        model.addAttribute("accountList", accNotifService.getAccounts());
        return "NotificationDev";
    }

    @RequestMapping(value = "/notif-dev/create-notif", method = RequestMethod.GET)
    public String createDevNotif(Model model){
        model.addAttribute("newNotifDev", new NotifModel());
        return "notifFormDev";
    }

    @RequestMapping(value = "/notif-dev/add-notif", method = RequestMethod.POST)
    public String addDevNotif(@ModelAttribute("notificatDev") NotifModel newNotif) {
        accNotifService.addNotification(newNotif);
        return "redirect:/notif-dev";
    }


}
