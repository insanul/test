package kelompok12.unus.notifierSbApp.Service;

import kelompok12.unus.notifierSbApp.Core.DBAccNotif;
import kelompok12.unus.notifierSbApp.Core.NotifModel;
import kelompok12.unus.notifierSbApp.Core.Person;
import kelompok12.unus.notifierSbApp.Repository.NotifRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DBAccNotifService implements DBAccNotifServiceInf {
    private final NotifRepo notifRepo;
    private final DBAccNotif dbAcc;
    private final Person account1;
    private final Person account2;
    private final Person account3;

    public DBAccNotifService(NotifRepo notifRepoIn){
        this.notifRepo = notifRepoIn;
        this.dbAcc = new DBAccNotif();
        this.account1 = new Person(dbAcc, "orang1");
        this.account2 = new Person(dbAcc, "orang2");
        this.account3 = new Person(dbAcc, "orang3");
        dbAcc.add(account1);
        dbAcc.add(account2);
        dbAcc.add(account3);
    }

    @Override
    public void addNotification(NotifModel notif) {
        dbAcc.addNotification(notif);
        notifRepo.save(notif);
    }

    @Override
    public List<Person> getAccounts() {
        return dbAcc.getAccounts();
    }
}
