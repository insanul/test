package kelompok12.unus.notifierSbApp.Service;

import kelompok12.unus.notifierSbApp.Core.NotifModel;
import kelompok12.unus.notifierSbApp.Core.Person;

import java.util.List;

public interface DBAccNotifServiceInf {
    void addNotification(NotifModel notif);
    List<Person> getAccounts();
}
