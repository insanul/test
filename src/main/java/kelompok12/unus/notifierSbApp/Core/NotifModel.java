package kelompok12.unus.notifierSbApp.Core;

public class NotifModel {
    private String title;
    private String desc;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDesc(String type) {
        this.desc = type;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDesc() {
        return this.desc;
    }

}
