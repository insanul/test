package kelompok12.unus.notifierSbApp.Core;

import java.util.ArrayList;
import java.util.List;

public class Person {
    protected String name;
    private List<NotifModel> notifs = new ArrayList<>();
    private DBAccNotif dbAcc;

    public Person(){
        this.name = "nama";
    }

    public Person(DBAccNotif dbAccIn, String nama){
        this.name = nama;
        this.dbAcc = dbAccIn;
    }

    public String getName() {
        return name;
    }

    public List<NotifModel> getNotifs() {
        return this.notifs;
    }

    public void addNotif(NotifModel notif){
        notifs.add(notif);
    }

    public void update() {
        this.getNotifs().add(dbAcc.getNotification());
    }
}
