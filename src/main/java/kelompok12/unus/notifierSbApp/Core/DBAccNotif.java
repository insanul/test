package kelompok12.unus.notifierSbApp.Core;

import java.util.ArrayList;
import java.util.List;

public class DBAccNotif {
    private List<Person> accounts = new ArrayList<>();
    private NotifModel notificat;

    public void add(Person account) {
        accounts.add(account);
    }

    public void addNotification(NotifModel notifIn) {
        this.notificat = notifIn;
        broadcast();
    }

    public NotifModel getNotification() {
        return notificat;
    }

    public List<Person> getAccounts() {
        return accounts;
    }

    private void broadcast() {
        for (Person acc : accounts) {
            acc.update();
        }
    }
}
