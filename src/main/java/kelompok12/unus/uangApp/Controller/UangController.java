package kelompok12.unus.uangApp.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(path = "/uang")
public class UangController {
    @RequestMapping(method = RequestMethod.GET, value = "/")
    private String uangMethod1() {
        return "home";
    }
    @RequestMapping(method = RequestMethod.GET, value = "/apalah")
    private String uangMethod2() {
        return "SplitBill";
    }
}
