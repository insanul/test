package kelompok12.unus.SecurityTest.Service;


import kelompok12.unus.Security.UserDetailsImpl;
import kelompok12.unus.accountApp.Model.BaseUser;
import kelompok12.unus.accountApp.Model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

public class UserDetailsImplTest {
    private UserDetailsImpl userDetails;

    @BeforeEach
    public void setUp() throws Exception {
        User user = new BaseUser("admin", "admin123", "USER");
        userDetails = new UserDetailsImpl(user);
    }

    @Test
    public void getPasswordShouldReturnAString() {
        assertNotNull(userDetails.getPassword());
    }

    @Test
    public void getPasswordShouldReturnUserDetailsPassword(){
        assertEquals("admin123", userDetails.getPassword());
    }

    @Test
    public void getUsernameShouldNotReturnNull() {
        assertNotNull(userDetails.getUsername());
    }

    @Test
    public void getUsernameShouldReturnUserDetailsUsername(){
        assertEquals("admin", userDetails.getUsername());
    }

    @Test
    public void isAccountNotExpiredSHouldReturnTrue(){
        assertTrue(userDetails.isAccountNonExpired());
    }

    @Test
    public void isAccountNotLockedShouldReturnTrue() {
        assertTrue(userDetails.isAccountNonLocked());
    }

    @Test
    public void isCredentialsNotExpiredShouldReturnTrue() {
        assertTrue(userDetails.isCredentialsNonExpired());
    }

    @Test
    public void isEnableShouldReturnTrue() {
        assertTrue(userDetails.isEnabled());
    }

    @Test
    public void getAuthoritiesShouldNotReturnNull(){
        assertNotNull(userDetails.getAuthorities());
    }
}
