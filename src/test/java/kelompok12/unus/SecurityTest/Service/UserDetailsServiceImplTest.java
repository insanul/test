package kelompok12.unus.SecurityTest.Service;

import kelompok12.unus.Security.UserDetailsServiceImpl;
import kelompok12.unus.accountApp.Repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class UserDetailsServiceImplTest {
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserDetailsServiceImpl userDetailsService;

    @Test
    @WithMockUser(username = "admin", roles = {"ADMIN"})
    public void loadUserByUsernameShouldCallUserRepositoryFindUserByName(){
        UserDetails user = userDetailsService.loadUserByUsername("admin");
        verify(userRepository, times(1)).findByUserName("admin");
    }
}
