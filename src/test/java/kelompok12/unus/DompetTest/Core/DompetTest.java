package kelompok12.unus.DompetTest.Core;

import kelompok12.unus.dompetApp.Core.Dompet;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DompetTest{
    @Test
    public void testobject1(){
        Dompet dompet = new Dompet("At","Uang kas atee");

        assertEquals("At", dompet.getName());
        assertEquals("Uang kas atee", dompet.getDescription());
    }
}