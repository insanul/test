package kelompok12.unus.notifierSbAppTest.Repository;

import kelompok12.unus.notifierSbApp.Core.NotifModel;
import kelompok12.unus.notifierSbApp.Repository.NotifRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class NotifRepoTest {
    private NotifRepo notifRepo;

    @BeforeEach
    public void setUp() {
        notifRepo = new NotifRepo();
    }

    @Test
    public void testSaveNewNotifShouldAddItToTheRepository() {
        NotifModel newNotif = new NotifModel();
        newNotif.setTitle("Test Notif title");
        newNotif.setDesc("Test Notif description");

        notifRepo.save(newNotif);
        Map<String, NotifModel> notifRepository = notifRepo.getNotifications();

        assertThat(notifRepository).hasSize(1);
        assertThat(notifRepository).containsValue(newNotif);
    }

    @Test
    public void testSaveExistingNotifWithGivenTitleShouldNotSaveItToTheRepository() {
        NotifModel savedNotif = new NotifModel();
        savedNotif.setTitle("Test Notif 1 title");
        savedNotif.setDesc("Test Notif 1 desc");
        notifRepo.save(savedNotif);
        NotifModel newNotif = new NotifModel();
        newNotif.setTitle("Test Notif 1 title");
        newNotif.setDesc("Test Notif 1 description");

        notifRepo.save(newNotif);
        Map<String, NotifModel> notifRepository = notifRepo.getNotifications();
        assertThat(notifRepository).size().isEqualTo(1);
        assertThat(notifRepository).size().isNotEqualTo(2);
    }
}
