package kelompok12.unus.notifierSbAppTest.Service;

import kelompok12.unus.notifierSbApp.Core.DBAccNotif;
import kelompok12.unus.notifierSbApp.Core.NotifModel;
import kelompok12.unus.notifierSbApp.Core.Person;
import kelompok12.unus.notifierSbApp.Repository.NotifRepo;
import kelompok12.unus.notifierSbApp.Service.DBAccNotifService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class DBAccNotifServiceTest {
    @Mock
    private NotifRepo notifRepo;

    @InjectMocks
    private DBAccNotifService accNotifService;

    @Test
    public void whenAddNotifIsCalledItShouldCallNotifRepoSave() {
        NotifModel notif = new NotifModel();
        notif.setTitle("Test Notif title");
        notif.setDesc("Test Notif description");
        accNotifService.addNotification(notif);
        verify(notifRepo, times(1)).save(notif);
    }

    @Test
    public void whenGetAccountsIsCalledItShouldReturnTheSameSize() {
        DBAccNotif dbAcc = new DBAccNotif();
        Person account1 = new Person(dbAcc, "orang1");
        Person account2 = new Person(dbAcc, "orang2");
        Person account3 = new Person(dbAcc, "orang3");
        dbAcc.add(account1);
        dbAcc.add(account2);
        dbAcc.add(account3);

        assertThat(accNotifService.getAccounts()).size().isEqualTo(3);
    }
}
