package kelompok12.unus.notifierSbAppTest.Controller;

import kelompok12.unus.notifierSbApp.Controller.NsbController;
import kelompok12.unus.notifierSbApp.Core.DBAccNotif;
import kelompok12.unus.notifierSbApp.Core.NotifModel;
import kelompok12.unus.notifierSbApp.Core.Person;
import kelompok12.unus.notifierSbApp.Service.DBAccNotifService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = NsbController.class)

public class NsbControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DBAccNotif dbAccNotif;

    @MockBean
    private DBAccNotifService dbAccNotifService;

    @Test
    public void whenSplitBillURLIsAccessedItShouldContainCorrectView() throws Exception {
        mockMvc.perform(get("/split-bill"))
                .andExpect(status().isOk())
                .andExpect(view().name("SplitBill"));
    }

    @Test
    public void whenNotifURLIsAccessedItShouldContainCorrectView() throws Exception {
        mockMvc.perform(get("/notif"))
                .andExpect(status().isOk())
                .andExpect(view().name("Notification"))
                .andExpect(model().attributeExists("notifList"));
    }

    @Test
    public void whenCreateNotifURLIsAccessedItShouldContainCorrectView() throws Exception {
        mockMvc.perform(get("/notif/create-notif"))
                .andExpect(status().isOk())
                .andExpect(view().name("notifForm"))
                .andExpect(model().attributeExists("newNotif"));
    }

    @Test
    public void whenAddNotifURLIsAccessedItShouldContainCorrectView() throws Exception {
        NotifModel newNotif = new NotifModel();
        newNotif.setTitle("NotifTitle");
        newNotif.setDesc("NotifDesc");

        mockMvc.perform(post("/notif/add-notif")
                .flashAttr("notificat", newNotif))
                .andExpect(handler().methodName("addNotif"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/notif"));
    }

    @Test
    public void testPerson() throws Exception {
        Person orang = new Person();
        assertEquals("nama", orang.getName());
    }

    @Test
    public void whenNotifDevURLIsAccessedItShouldContainCorrectView() throws Exception {
        mockMvc.perform(get("/notif-dev"))
                .andExpect(status().isOk())
                .andExpect(view().name("NotificationDev"))
                .andExpect(model().attributeExists("accountList"));
    }

    @Test
    public void whenCreateDevNotifURLIsAccessedItShouldContainCorrectView() throws Exception {
        mockMvc.perform(get("/notif-dev/create-notif"))
                .andExpect(status().isOk())
                .andExpect(view().name("notifFormDev"))
                .andExpect(model().attributeExists("newNotifDev"));
    }

    @Test
    public void whenAddDevNotifURLIsAccessedItShouldContainCorrectView() throws Exception {
        NotifModel newNotif = new NotifModel();
        newNotif.setTitle("NotifTitle");
        newNotif.setDesc("NotifDesc");

        mockMvc.perform(post("/notif-dev/add-notif")
                .flashAttr("notificatDev", newNotif))
                .andExpect(handler().methodName("addDevNotif"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/notif-dev"));
    }
}
