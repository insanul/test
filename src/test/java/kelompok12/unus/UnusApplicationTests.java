package kelompok12.unus;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UnusApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	public void mainFunctionTest() {
		UnusApplication.main(new String[] {});
	}

}
